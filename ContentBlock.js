import React from 'react';
import PropTypes from 'prop-types';

//import RichText from 'richtext';
//import Heading from 'heading';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';
import Button from 'button';
import LinkButton from 'button/linkbutton';
import Figure from 'figure';

import {getModifiers} from 'libs/component';

import './ContentBlock.scss';

/**
 * ContentBlock
 * @description [Description]
 * @example
  <div id="ContentBlock"></div>
  <script>
    ReactDOM.render(React.createElement(Components.ContentBlock, {
    	title : 'Example ContentBlock'
    }), document.getElementById("ContentBlock"));
  </script>
 */
class ContentBlock extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = props.baseClass;
	}

	renderLinks() {
		const {cta} = this.props;

		if (!cta) {
			return null;
		}

		return Array.isArray(cta)
			? cta.map((item, i) => {
					return item.type === 'button' ? (
						<Button key={`link-${i}`} {...item} />
					) : (
						<LinkButton key={`link-${i}`} {...item} />
					);
			  })
			: [cta.type === 'button' ? <Button {...cta} key="button" /> : <LinkButton {...cta} key="button" />];
	}

	render() {
		const {title, content, titleSize, href, image, modifier} = this.props;

		const links = this.renderLinks();

		return (
			<div className={getModifiers(this.baseClass, [modifier])} ref={component => (this.component = component)}>
				<Figure image={image} />
				<div className={`${this.baseClass}__body`}>
					<Heading content={title} h={titleSize} href={href} />
					<RichText content={content} />
					{links && <div className={`${this.baseClass}__links`}>{links}</div>}
				</div>
			</div>
		);
	}
}

ContentBlock.defaultProps = {
	baseClass: 'content-block',
	modifier: null,
	image: null,
	href: '',
	titleSize: 2,
	title: '',
	content: '',
	cta: null
};

ContentBlock.propTypes = {
	baseClass: PropTypes.string.isRequired,
	modifier: PropTypes.string,
	image: PropTypes.object,
	heading: PropTypes.number,
	title: PropTypes.string,
	content: PropTypes.string,
	cta: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
};

export default ContentBlock;
